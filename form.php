<?php


    $db_host = "localhost";
    $db_user = "root";
    $db_password = '';
    $db_name = "registration_form";

    $connect = mysqli_connect($db_host, $db_user, $db_password, $db_name);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if(!empty($_POST['f_name']) && !empty($_POST['l_name']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['password']) && !empty($_POST['r_password'])){
            $f_name = $_POST['f_name'];
            $l_name = $_POST['l_name'];
            $email = $_POST['email'];
            $password = $_POST['phone'];
            $password = $_POST['password'];
            $password = $_POST['r_password'];
    
            $query = "insert db(First Name, Last Name, Email, Phone, Password, Repeat Password) values('$f_name','$l_name','$email','$phone','$password','$r_password')";
    
            $run = mysqli_query($conn,$query) or die(mysqli_error($conn));
    
            if($run){
                echo "Form submitted successfully";
            }else{
                echo "Form not submitted";
            }
        }
        else{
            echo "all fields required";
        }

    }
    // if($pdoconnect)
    // {
    //     echo "Connected";
    // }
    // else{
    //     echo "Not Connected";
    // }

    // $sql = "Select * FROM `db`";
    // $stml = $pdoconnect->query($sql);
    // $data = $stml->fetchAll();
?>



<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Form</title>
  </head>
  <body>

        <h3 class="text-center m-3">Student Registration Form :</h3>

        <hr class="bg-success">

        <div class="container" style="background-color:#ddd;">

            <div class="row d-flex justify-content-center align-items-center" style="height:600px;">
                    <div class="card w-50 shadow-lg p-3 mb-5 bg-light mx-auto" style="border-radius:20px;">
                            <div class="card-body">
                                <fieldset>
                                        <legend>Fill up this form :</legend>

                                        <form action="" method="POST">
                                            <div class="row mb-3">
                                                    <div class="col-12">
                                                        <label for="f_name" class="float-start form-label">First Name:</label>
                                                        <input type="text" name="f_name" id="f_name" class="f_name w-75 float-end form-control" placeholder="enter your first name here...." required>
                                                    </div>
                                            </div>

                                            <div class="row mb-3">
                                                    <div class="col-12">
                                                        <label for="l_name" class="float-start form-label">Last Name:</label>
                                                        <input type="text" name="l_name" id="l_name" class="l_name w-75 float-end form-control" placeholder="enter your last name here...." required>
                                                    </div>
                                            </div>

                                            <div class="row mb-3">
                                                    <div class="col-12">
                                                        <label for="email" class="float-start form-label">Email:</label>
                                                        <input type="email" name="email" id="email" class="email w-75 float-end form-control" placeholder="enter your email name here...." required>
                                                    </div>
                                            </div>

                                            <div class="row mb-3">
                                                    <div class="col-12">
                                                        <label for="phone" class="float-start form-label">Phone:</label>
                                                        <input type="number" name="phone" id="email" class="phone w-75 float-end form-control" placeholder="enter your contact here...." required>
                                                    </div>
                                            </div>

                                             <div class="row mb-3">
                                                    <div class="col-12">
                                                        <label for="password" class="float-start form-label">Password:</label>
                                                        <input type="password" name="password" id="password" class="password form-control w-75 ml-3 float-end" placeholder="enter your password here...." required>
                                                    </div>
                                            </div>

                                            <div class="row mb-3">
                                                    <div class="col-12">
                                                        <label for="r_password" class="float-start form-label">Repeat Password:</label>
                                                        <input type="password" name="r_password" id="r_password" class="r_password form-control w-75 ml-3 float-end" placeholder="repeat your password here...." required>
                                                    </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-12">
                                                <button class="btn btn-outline-success w-100" type="submit">Register</button>
                                                </div>
                                            </div>
                                        </form>
                                </fieldset>
                            </div>
                    </div>
            </div>

        </div>









        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>